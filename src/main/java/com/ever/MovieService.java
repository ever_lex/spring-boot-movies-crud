package com.ever;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;


@Service
public class MovieService {

    @Autowired
    MovieRepository movieRepository;

    public Optional<Movie> findById(@NotNull Integer id) {
        return movieRepository.findById(id);
    }

    public Movie save(@NotNull Movie movie) {
        return movieRepository.save(movie);
    }

    public void deleteById(@NotNull Integer id) {
        movieRepository.deleteById(id);
    }

    public Iterable<Movie> findAll() {
        return movieRepository.findAll();
    }

    public Movie update(Movie movie) {

        Movie movie1 = movieRepository.findById(movie.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Movie with id " +  movie.getId() + " not found!"));

        movie1.setTitle(movie.getTitle());
        movie1.setYear(movie.getYear());
        movie1.setGenre(movie.getGenre());
        return movieRepository.save(movie1);
    }
}