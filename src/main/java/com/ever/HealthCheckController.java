package com.ever;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class HealthCheckController {

    @GetMapping
    public String home(){
        return "Spring Boot UP! ";
    }

}
