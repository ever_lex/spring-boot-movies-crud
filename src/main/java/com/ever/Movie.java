package com.ever;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.function.Consumer;

@Entity
@Table(name = "movie")
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "year", nullable = false)
    private Integer year;

    @Column(name = "genre", nullable = false)
    private String genre;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public String getGenre() {
        return genre;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    static public Movie create(Consumer<MovieBuilder> buildingFunction) {
        return new Movie().build(buildingFunction);
    }

    private Movie build(Consumer<MovieBuilder> buildingFunction){
        buildingFunction.accept(new MovieBuilder() {
            @Override
            public MovieBuilder withtTitle(String title) {
                Movie.this.title = title;
                return this;
            }

            @Override
            public MovieBuilder withYear(Integer year) {
                Movie.this.year = year;
                return this;
            }

            @Override
            public MovieBuilder withGenre(String genre) {
                Movie.this.genre = genre;
                return this;
            }
        });

        return this;
    }

    public interface MovieBuilder {

        MovieBuilder withtTitle(String title);

        MovieBuilder withYear(Integer year);

        MovieBuilder withGenre(String genre);
    }
}
