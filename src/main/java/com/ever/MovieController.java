package com.ever;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;

@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    MovieService movieService;

    @GetMapping("/{id}")
    public Movie findByModalityId(@PathVariable @NotNull Integer id) {
        return movieService.findById(id).orElse(null);
    }

    @GetMapping
    public Iterable<Movie> list() {
        return movieService.findAll();
    }

    @PutMapping
    public Movie update(@RequestBody @Valid Movie movie) {
        return movieService.update(movie);
    }

    @PostMapping
    public Movie save(@RequestBody @Valid Movie movie){
        return movieService.save(movie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable @NotNull Integer id) {
        movieService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}